﻿using System.Linq;

namespace IPP.Recruitment.Domain.BusinessRules
{
    /// <summary>
    /// Performs a Mod-10/LUHN check on the passed number and returns true if the check passed
    /// </summary>
    /// <param name="cardNumber">A 16 digit card number</param>
    /// <returns>true if the card number is valid, otherwise false</returns>
    /// <remarks>
    /// Refer here for MOD10 algorithm: https://en.wikipedia.org/wiki/Luhn_algorithm
    /// </remarks>
    public class IsCardNumberValid : IBusinessRule
    {
        private readonly string _cardNumber;

        public IsCardNumberValid(string cardNumber)
        {
            _cardNumber = cardNumber;
        }
        public bool Validate()
        {
            if (string.IsNullOrEmpty(_cardNumber) || _cardNumber.ToCharArray().Count() != 16)
                return false;

            int i, checkSum = 0;

            // Compute checksum of every other digit starting from right-most digit
            for (i = _cardNumber.Length - 1; i >= 0; i -= 2)
                checkSum += (_cardNumber[i] - '0');

            // Now take digits not included in first checksum, multiple by two,
            // and compute checksum of resulting digits
            for (i = _cardNumber.Length - 2; i >= 0; i -= 2)
            {
                int val = ((_cardNumber[i] - '0') * 2);
                while (val > 0)
                {
                    checkSum += (val % 10);
                    val /= 10;
                }
            }

            // Number is valid if sum of both checksums MOD 10 equals 0
            return ((checkSum % 10) == 0);
        }
    }
}