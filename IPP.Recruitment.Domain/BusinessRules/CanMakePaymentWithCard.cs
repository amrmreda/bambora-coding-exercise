﻿using System;
using System.Linq;

namespace IPP.Recruitment.Domain.BusinessRules
{
    public class CanMakePaymentWithCard : IBusinessRule
    {
        private readonly string _cardNumber;
        private int _expiryMonth;
        private int _expiryYear;

        /// <summary>
        /// Validates the card number, expiry motnh and year to ensure the details can be used to make a payment
        /// </summary>
        /// <param name="cardNumber">A 16 digit card number</param>
        /// <param name="expiryMonth">Month part of the expiry date</param>
        /// <param name="expiryYear">Year part of the expiry date</param>
        /// <returns>true if the details represent a valid card, otherwise false</returns>
        /// <remarks>
        /// Validations:
        /// cardNumber: Ensure the passed string is 16 in length and passes the MOD10/LUHN check
        /// expiryMonth: should represent a month number between 1 and 12
        /// expiryYear: Should represent a year value, 4 characters in lenght and either the current or a future year
        /// The expiry month + year should represent a date in the future
        /// </remarks>
        public CanMakePaymentWithCard(string cardNumber, int expiryMonth, int expiryYear)
        {
            _cardNumber = cardNumber;
            _expiryMonth = expiryMonth;
            _expiryYear = expiryYear;
        }
        public bool Validate()
        {
            return IsCardNumberValid(_cardNumber) && IsExpiryDateValid(_expiryMonth, _expiryYear);   
        }

        private bool IsCardNumberValid(string cardNumber)
        {
            if (string.IsNullOrEmpty(_cardNumber) || _cardNumber.ToCharArray().Count() != 16)
                return false;

            int i, checkSum = 0;

            // Compute checksum of every other digit starting from right-most digit
            for (i = _cardNumber.Length - 1; i >= 0; i -= 2)
                checkSum += (_cardNumber[i] - '0');

            // Now take digits not included in first checksum, multiple by two,
            // and compute checksum of resulting digits
            for (i = _cardNumber.Length - 2; i >= 0; i -= 2)
            {
                int val = ((_cardNumber[i] - '0') * 2);
                while (val > 0)
                {
                    checkSum += (val % 10);
                    val /= 10;
                }
            }

            // Number is valid if sum of both checksums MOD 10 equals 0
            return ((checkSum % 10) == 0);


            return true;
        }
        private bool IsExpiryDateValid(int expiryMonth, int expiryYear)
        {
            if (expiryMonth < 1 || expiryMonth > 12)
                throw new ArgumentOutOfRangeException("expiryMonth");

            if (expiryYear < DateTime.Now.Year || expiryYear > 9999)
                throw new ArgumentOutOfRangeException("expiryYear");

            var lastDateOfExpiryMonth = DateTime.DaysInMonth(expiryYear, expiryMonth); //get actual expiry date
            var cardExpiry = new DateTime(expiryYear, expiryMonth, lastDateOfExpiryMonth, 23, 59, 59);

          return  cardExpiry > DateTime.Now;
        }

        
    }
}