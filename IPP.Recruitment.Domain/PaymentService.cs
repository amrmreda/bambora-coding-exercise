﻿using IPP.Recruitment.Domain.BusinessRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPP.Recruitment.Domain
{
    public class PaymentService : IPaymentService
    {
        public Guid MakePayment(CreditCard creditCard)
        {
            var rules = new List<IBusinessRule>
            {
                new CanMakePaymentWithCard(creditCard.CardNumber, creditCard.ExpiryMonth, creditCard.ExpiryYear),
                new IsCardNumberValid(creditCard.CardNumber),
                new IsValidPaymentAmount(creditCard.Amount)
            };
            var IsValid = rules.Any((e) => e.Validate());

            if (IsValid)
                return Guid.NewGuid();
            else
               return Guid.Empty;
        }

        public string WhatsYourId()
        {
            return "008ab27c-36b2-43e5-91d5-edbd1e5b564b";
        }
    }
}
