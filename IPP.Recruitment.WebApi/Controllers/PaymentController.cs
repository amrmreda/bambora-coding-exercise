﻿using IPP.Recruitment.Domain;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPP.Recruitment.WebApi.Controllers
{ 
    public class PaymentController : ApiController
    {
        IPaymentService _paymentService;

        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }


        // GET: api/payment/WhatsYourId
        [HttpGet]
        [Route("api/payment/WhatsYourId")]
        public string WhatsYourId()
        {
            return _paymentService.WhatsYourId();
        }

        // POST: api/payment/MakePayment/4111111111111111/3/2019/999999
        [HttpPost]
        [Route("api/payment/MakePayment/{cardNumber}/{expiryMonth}/{expiryYear}/{amount}")]
        public Guid MakePayment(string cardNumber, int expiryMonth, int expiryYear, long amount)
        {
            try
            {
                var creditCard = new CreditCard() {
                    CardNumber = cardNumber,
                    ExpiryMonth = expiryMonth,
                    ExpiryYear = expiryYear,
                    Amount = amount
                };

                return _paymentService.MakePayment(creditCard);
            }
            catch (ArgumentNullException ane)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ane.Message));
            }
            catch (ArgumentException ae)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ae.Message));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }
        }
    }
}
