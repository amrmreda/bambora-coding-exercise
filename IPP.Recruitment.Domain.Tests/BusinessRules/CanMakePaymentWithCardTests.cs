﻿using IPP.Recruitment.Domain.BusinessRules;
using NUnit.Framework;
using System;

namespace IPP.Recruitment.Domain.Tests.BusinessRules
{
    [TestFixture]
    public class CanMakePaymentWithCardTests
    {
        [Test]
        public void Validate_GivenValidCardSchema_ShouldReturnTrue()
        {
            var validCardNumber = "5555555555554444";
            var validExpiryYear = DateTime.Now.AddYears(2).Year;
            var validExpiryMonth = 12; 

            var canMakePaymentWithCard = new CanMakePaymentWithCard(validCardNumber, validExpiryMonth, validExpiryYear);


            Assert.IsTrue(canMakePaymentWithCard.Validate());
        }


        [Test]
        public void ShouldValidate_CardNumberIsValid_ReturnTrue()
        {
            var isCardNumberValid = new IsCardNumberValid("5555555555554444");

            Assert.IsTrue(isCardNumberValid.Validate());
        }

        [Test]
        public void ShouldValidate_CardNumberIsEmpty_ReturnFalse()
        {
            var isCardNumberValid = new IsCardNumberValid(string.Empty);

            Assert.IsFalse(isCardNumberValid.Validate());
        }

        [Test]
        public void ShouldValidate_CardNumberIsNot16Digit_ReturnFalse()
        {
            var isCardNumberValid = new IsCardNumberValid("6006491794221");

            Assert.IsFalse(isCardNumberValid.Validate());
        }

        [Test]
        public void ShouldValidate_CreditCardNumberFailed10ModChecksum_ReturnFalse()
        {
            var isCardNumberValid = new IsCardNumberValid("600649179422141340");

            Assert.IsFalse(isCardNumberValid.Validate());
        }

        [Test]
        public void Validate_GivenInvalidExpiryYear_ShouldThrowArgumentOutOfRangeException_Case1()
        {
            var validCardNumber = "5555555555554444";
            var validExpiryYear = DateTime.Now.AddYears(-2).Year;
            var validExpiryMonth = 12;

            var canMakePaymentWithCard = new CanMakePaymentWithCard(validCardNumber, validExpiryMonth, validExpiryYear);


            Assert.Throws<ArgumentOutOfRangeException>(() => canMakePaymentWithCard.Validate());
        }

        [Test]
        public void Validate_GivenInvalidExpiryYear_ShouldThrowArgumentOutOfRangeException_Case2()
        {
            var validCardNumber = "5555555555554444";
            var validExpiryYear = 10000;
            var validExpiryMonth = 12;

            var canMakePaymentWithCard = new CanMakePaymentWithCard(validCardNumber, validExpiryMonth, validExpiryYear);


            Assert.Throws<ArgumentOutOfRangeException>(() => canMakePaymentWithCard.Validate());
        }


        [Test]
        public void Validate_GivenInvalidExpiryMonth_ShouldThrowArgumentOutOfRangeException_Case1()
        {
            var validCardNumber = "5555555555554444";
            var validExpiryYear = DateTime.Now.AddYears(2).Year;
            var validExpiryMonth = 13;

            var canMakePaymentWithCard = new CanMakePaymentWithCard(validCardNumber, validExpiryMonth, validExpiryYear);


            Assert.Throws<ArgumentOutOfRangeException>(() => canMakePaymentWithCard.Validate());
        }

        [Test]
        public void Validate_GivenInvalidExpiryMonth_ShouldThrowArgumentOutOfRangeException_Case2()
        {
            var validCardNumber = "5555555555554444";
            var validExpiryYear = DateTime.Now.AddYears(2).Year;
            var validExpiryMonth = -1;

            var canMakePaymentWithCard = new CanMakePaymentWithCard(validCardNumber, validExpiryMonth, validExpiryYear);


            Assert.Throws<ArgumentOutOfRangeException>(() => canMakePaymentWithCard.Validate());
        }


         
    }
}
