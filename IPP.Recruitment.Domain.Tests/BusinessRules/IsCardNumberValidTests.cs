﻿using IPP.Recruitment.Domain.BusinessRules;
using NUnit.Framework;

namespace IPP.Recruitment.Domain.Tests.BusinessRules
{
    [TestFixture]
    public class IsCardNumberValidTests
    {
        [Test]
        public void ShouldValidate_CardNumberIsValid_ReturnTrue()
        {
            var isCardNumberValid = new IsCardNumberValid("5555555555554444");

            Assert.IsTrue(isCardNumberValid.Validate());
        }

        [Test]
        public void ShouldValidate_CardNumberIsEmpty_ReturnFalse()
        {
            var isCardNumberValid = new IsCardNumberValid(string.Empty);

            Assert.IsFalse(isCardNumberValid.Validate());
        }

        [Test]
        public void ShouldValidate_CardNumberIsNot16Digit_ReturnFalse()
        {
            var isCardNumberValid = new IsCardNumberValid("6006491794221");

            Assert.IsFalse(isCardNumberValid.Validate());
        }

        [Test]
        public void ShouldValidate_CreditCardNumberFailed10ModChecksum_ReturnFalse()
        {
            var isCardNumberValid = new IsCardNumberValid("600649179422141340");

            Assert.IsFalse(isCardNumberValid.Validate());
        }

    }
}
