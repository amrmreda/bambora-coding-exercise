﻿using IPP.Recruitment.Domain.BusinessRules;
using NUnit.Framework; 

namespace IPP.Recruitment.Domain.Tests.BusinessRules
{
    [TestFixture]
    public class IsValidPaymentAmountTests
    {
        [Test]
        public void ShouldValidate_AmountIsWithinTheRange_ReturnTrue()
        {
            var validPaymentAmount = new IsValidPaymentAmount(100000);
            Assert.IsTrue(validPaymentAmount.Validate());
        }


        [Test]
        public void ShouldValidate_AmountIsWithinTheRange_ReturnFalse()
        {
            var validPaymentAmount = new IsValidPaymentAmount(98);

            Assert.IsFalse(validPaymentAmount.Validate());
        }

        [Test]
        public void ShouldValidate_AmountIsLargerThan99999999cent_ReturnFalse()
        {
            var validPaymentAmount = new IsValidPaymentAmount(100000000);

            Assert.IsFalse(validPaymentAmount.Validate());
        }
    }
}
