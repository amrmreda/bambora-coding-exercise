﻿using System; 
using NUnit.Framework;

namespace IPP.Recruitment.Domain.Tests
{
    [TestFixture]
    public class PaymentServiceTests
    {
        IPaymentService _paymentService;

        [SetUp]
        public void Setup()
        {
            _paymentService = new PaymentService();
        }

        [Test]
        public void ShouldWhatsYourId_ReturnGivenGUID()
        {
            var result = _paymentService.WhatsYourId();
            var expected = "008ab27c-36b2-43e5-91d5-edbd1e5b564b";

            Assert.AreEqual(expected, result);
        }


        [Test]
        public void ShouldMakePayment_ValidateCreditCardNumberUsingMod10Alg_ReturnValidGuid()
        {
            var validCardNumber = "5555555555554444";
            var validExpiryYear = DateTime.Now.AddYears(2);
            var validExpiryMonth = 12;
            
            var creditCard = new CreditCard()
            {
                CardNumber = validCardNumber,
                Amount = 1200,
                ExpiryMonth = validExpiryMonth,
                ExpiryYear = validExpiryYear.Year
            };

            var result = _paymentService.MakePayment(creditCard);

        }
    }
}
